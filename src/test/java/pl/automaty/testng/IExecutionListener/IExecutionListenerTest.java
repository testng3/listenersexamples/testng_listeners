package pl.automaty.testng.IExecutionListener;

import org.testng.annotations.Test;

public class IExecutionListenerTest {

    @Test
    public void testMethod01() {
        System.out.println("This is method 01.");
    }
@Test
    public void testMethod02() {
        System.out.println("This is method 02.");
    }
@Test
    public void testMethod03() {
        System.out.println("This is method 03.");
    }
@Test
    public void testMethod04() {
        System.out.println("This is method 04.");
    }

}
