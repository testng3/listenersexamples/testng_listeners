package pl.automaty.testng.IExecutionListener;

import org.testng.IExecutionListener;

public class IExListener implements IExecutionListener {

    @Override
    public void onExecutionStart() {
        IExecutionListener.super.onExecutionStart();
        System.out.println("TEST SUITE HAS STARTED");
    }

    @Override
    public void onExecutionFinish() {
        IExecutionListener.super.onExecutionFinish();
        System.out.println("TEST SUITE ENDED");
    }
}
