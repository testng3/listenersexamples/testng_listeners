## Implementacja słuchaczy
Słuchaczy w TestNG możemy zaimplementować na dwa sposoby:

1. Jako adnotację dodaną w klasie testowej przed słowem kluczowym **class**.

```
@Listeners(IHook.class)
public class ListenerTest {

...
}
```
Jeśli projekt ma wiele klas dodawanie słuchacy do każdej z nich za pomocą adnotacji może być kłopotliwe. W takich przypadkach możemy zrobić to w inny sposób.


2. Możemy utwożyć plik konfiguracjny **testng.xml** i w nim umieścić znacznik `<listeners>`:
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE suite SYSTEM "http://testng.org/testng-1.0.dtd">
<suite name="Test_suite">
    <listeners>
        <listener class-name="pl.automaty.testng.IHookable.IHook"/>
    </listeners>
    <test name="IHookable">
        <classes>
            <class name="pl.automaty.testng.tests.ListenerTest" />
        </classes>
    </test>
</suite>
```
w takim przypadku słuchacz będzie miał zastosowanie do całego zestawu testowego, niezależnie od liczby klas.
##
